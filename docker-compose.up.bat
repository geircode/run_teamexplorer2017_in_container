cd /d %~dp0
REM docker rm -f teamexplorer2017
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build
pause
docker exec -it teamexplorer2017 cmd