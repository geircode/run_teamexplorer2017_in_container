# escape=`


FROM microsoft/dotnet-framework

RUN @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
RUN chocolatey feature enable -n allowGlobalConfirmation
RUN choco install teamexplorer2017

ADD ./setpath.ps1 C:/MyScripts/setpath.ps1
RUN powershell -Command C:\MyScripts\setpath.ps1

ENTRYPOINT powershell Wait-Event